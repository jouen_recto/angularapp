app.controller('detailController', function(base_url,$scope, $http, $timeout, $route, $routeParams) {
    
    //var title = $route.current.$$route.title;
    //session.isLogin(title);
    $("#ng-side-nav").show();

    // $scope.navbar = base_url+'app/shared/navbar.html';
    var id = $routeParams.product_id;
    
    var dataObj = $.param({
      id : id 
    });
    var config = {
        headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    };

    $http.post('http://c3mailer.com/amazonApp/ngrequest/trackRequest',dataObj,config).then(successCallback, errorCallback);

    // $http.get('http://c3mailer.com/amazonApp/product/trackRequest/'+id).then(successCallback, errorCallback);

    function successCallback(xdata,status, headers, config){
    //success code
      $scope.details = xdata.data.product_details;
      $scope.price = JSON.parse(xdata.data.product_details.price);
      $scope.diff = xdata.data.diff;
    }

    function errorCallback(error){
    }
});

app.filter("trust", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  };
}]);

