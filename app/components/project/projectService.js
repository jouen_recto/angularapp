var projectService = angular.module('projectService', [])
.service('project', function (request_url,$http,$window) {
    var config = {
        headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    };
    this.addProject = function (project_name) {
         var user_data = JSON.parse($window.sessionStorage.user_data);
        var data = $.param({
            project_name: project_name,
            user_id:user_data.user_data.id
        });
        var responseProject = $http.post(request_url+'Ngrequest/submitCreateProject',data,config).then(function (response) {
            return response.data;
        });

        return responseProject;
    };
    this.getProject = function () {
         var user_data = JSON.parse($window.sessionStorage.user_data);
        var data = $.param({
            user_id:user_data.user_data.id
        });
        var responseProject = $http.post(request_url+'Ngrequest/getProject',data,config).then(function (response) {
            return response.data;
        });

        return responseProject;
    };

    this.addItem = function (project_id,item_id) {
         var user_data = JSON.parse($window.sessionStorage.user_data);
        var data = $.param({
            project_id: project_id,
            item_id:item_id
        });
        var responseProject = $http.post(request_url+'Ngrequest/addToProject',data,config).then(function (response) {
            return response.data;
        });

        return responseProject;
    };

    this.removeItem = function(item_id,project_id){
        var data = $.param({
            project_id: project_id,
            item_id:item_id
        });
        var responseProject = $http.post(request_url+'Ngrequest/deleteProjectItem',data,config).then(function (response) {
            return response.data;
        });

        return responseProject;
    };

});
