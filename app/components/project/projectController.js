app.controller('projectController', function(project,base_url,session,$scope,$location,$route,$routeParams, $window) {
	var title = $route.current.$$route.title;
    session.isLogin(title);
    $("#ng-side-nav").show();

    $scope.request_msg = false;

    $scope.addNewProject = function(){
    	if($("#project-name").val().replace(/\s/g,'') != "")
    	{
    		$scope.request_msg = false;
    		var project_name = $("#project-name").val();
    		project.addProject(project_name).then(function(response){
    			if(response.status)
    			{
    				$scope.request_msg = true;
		    		$scope.request_class = 'alert-success';
		    		$scope.request_text = 'new project successfully added';
		    		$("#project-name").val('');
		    		var last_child = $("#display-projects li:last-child");
		    		response.project.forEach(function(proj, index){
                    	last_child.before('<li><a href="#/project/'+proj.id+'">'+proj.name+'</a></li>');
                  	});
    			}
    		});


    	}else
    	{
    		$scope.request_msg = true;
    		$scope.request_class = 'alert-danger';
    		$scope.request_text = 'Please put a project name';

    	}
    };
  
});
app.controller('projectViewController', function($http,request_url,category,project,base_url,session,$scope,$location,$route,$routeParams, $window) {
  	var title = $route.current.$$route.title;
    session.isLogin(title);
    $("#ng-side-nav").show();

    //set advance search
    var main_table;
    var init_datatable = 0;

    //set category
   	category.getCategory().then(function(cat){
    	$scope.category = cat;
   	});

   	$http.get(request_url+'Ngrequest/getProjectItem/'+$routeParams.project_id).then(successCallback, errorCallback);


    function successCallback(data){
    //success code
    	$scope.content = data;
	}

	function errorCallback(error){
	    //error code
        alert('fetching data failed');
	}

 	$scope.$on('finishLoadItem', function(ngRepeatFinishedEvent) {
        //you also get the actual event object
        //do stuff, execute functions -- whatever...

        $.fn.DataTable.ext.search.push(
	        function( settings, data, dataIndex ) {

	            var result_price = false;
	            var result_rank = false;
	            var result_weight = false;
	            var result_cat = false;

	            var min = parseInt($('#item-min-price').val());
	            var max = parseInt($('#item-max-price').val());
	            var price = parseInt( data[5] ) || 0; // use data for the price column
	            
	            if ( ( isNaN( min ) && isNaN( max ) ) ||
	                 ( isNaN( min ) && price <= max ) ||
	                 ( min <= price   && isNaN( max ) ) ||
	                 ( min <= price   && price <= max ) )
	            {
	                result_price = true;
	            }

	            var min = parseInt($('#item-min-rank').val());
	            var max = parseInt($('#item-max-rank').val());
	            var rank = parseInt( data[6] ) || 0; // use data for the rank column
	            
	            if ( ( isNaN( min ) && isNaN( max ) ) ||
	                 ( isNaN( min ) && rank <= max ) ||
	                 ( min <= rank   && isNaN( max ) ) ||
	                 ( min <= rank   && rank <= max ) )
	            {
	                result_rank = true;
	            }

	            var min = parseInt($('#item-min-wieght').val());
	            var max = parseInt($('#item-max-wieght').val());
	            var wieght = parseInt( data[7] ) || 0; // use data for the wieght column
	            
	            if ( ( isNaN( min ) && isNaN( max ) ) ||
	                 ( isNaN( min ) && wieght <= max ) ||
	                 ( min <= wieght   && isNaN( max ) ) ||
	                 ( min <= wieght   && wieght <= max ) )
	            {
	                result_weight = true;
	            }

	            // console.log('selected cat');
	            // console.log($("#item-category").val());
	            // console.log($("#item-category").val() != "default");
	            // console.log('item cat');
	            // console.log(data[8]);
	            if($("#item-category").val() != "default")
	            {
	                var cat_id = data[8];
	                if($("#item-category").val() == cat_id)
	                {
	                    result_cat = true;
	                }
	            }else
	            {
	                result_cat = true;
	            }

	            if(result_price && result_rank && result_weight && result_cat)
	            {
	                return true;
	            }

	            return false;
	        }
	    );
        if ( ! $.fn.DataTable.isDataTable( '#product-list-project' ) ) {
	        main_table = $("#product-list-project").DataTable({
	            "columnDefs": [
	                // { "width": "60%", "targets": 3 },
	                {
	                    "targets": [ 4 ],
	                    "visible": false
	                },
	                {
	                    "targets": [ 5 ],
	                    "visible": false
	                },
	                {
	                    "targets": [ 6 ],
	                    "visible": false
	                },
	            ],
	        });
	    }

        $("#btn-search-prod").click(function(){
            
            main_table.search($("#item-title").val()).draw();
        });

    });

    $scope.removeProjectItem = function(item_id){
    	notie.confirm({
		  text: "Are you sure to remove this from this project?",
		  submitText: "Remove", // optional, default = 'Yes'
		  cancelText: 'Cancel', // optional, default = 'Cancel'
		  submitCallback: function(){

		  	project.removeItem(item_id,$routeParams.project_id).then(function(response){
		  		if(response.status)
		  		{
		  			$(".project-item-"+item_id).remove();
		  		}
		  	});
		  	
		  }
		});
    }


}).directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});

app.filter("trust", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);