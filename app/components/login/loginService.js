var googleService = angular.module('googleService', [])
.service('google', function (request_url,$http) {
    this.getUrl = function () {
      var google_url = $http.get(request_url+'login/getGoogleUrl').then(function (response) {
          return response.data;
      });

      return google_url;
    };

    this.validateCode = function(code) {
    	var data = $.param({
            code: code
        });
        var config = {
            headers : {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        }
	 	var validate = $http.post(request_url+'login/ngUrl',data,config).then(function (response) {
          	return response.data;
      	});

      	return validate;
    };
});
