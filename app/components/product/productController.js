app.controller('productController', function(project,category,session,base_url_pure,$scope, $http,$route, $timeout) {
    var title = $route.current.$$route.title;
    session.isLogin(title);
    $("#ng-side-nav").show();

    //get project
    project.getProject().then(function(response){
        $scope.isProject = response.status;
        $scope.projectList = response.projectList;
       
    });

    //set advance search
    var main_table;
    var init_datatable = 0;
    $.fn.DataTable.ext.search.push(
        function( settings, data, dataIndex ) {

            var result_price = false;
            var result_rank = false;
            var result_weight = false;
            var result_cat = false;

            var min = parseInt($('#item-min-price').val());
            var max = parseInt($('#item-max-price').val());
            var price = parseInt( data[5] ) || 0; // use data for the price column
            
            if ( ( isNaN( min ) && isNaN( max ) ) ||
                 ( isNaN( min ) && price <= max ) ||
                 ( min <= price   && isNaN( max ) ) ||
                 ( min <= price   && price <= max ) )
            {
                result_price = true;
            }

            var min = parseInt($('#item-min-rank').val());
            var max = parseInt($('#item-max-rank').val());
            var rank = parseInt( data[6] ) || 0; // use data for the rank column
            
            if ( ( isNaN( min ) && isNaN( max ) ) ||
                 ( isNaN( min ) && rank <= max ) ||
                 ( min <= rank   && isNaN( max ) ) ||
                 ( min <= rank   && rank <= max ) )
            {
                result_rank = true;
            }

            var min = parseInt($('#item-min-wieght').val());
            var max = parseInt($('#item-max-wieght').val());
            var wieght = parseInt( data[7] ) || 0; // use data for the wieght column
            
            if ( ( isNaN( min ) && isNaN( max ) ) ||
                 ( isNaN( min ) && wieght <= max ) ||
                 ( min <= wieght   && isNaN( max ) ) ||
                 ( min <= wieght   && wieght <= max ) )
            {
                result_weight = true;
            }

            // console.log('selected cat');
            // console.log($("#item-category").val());
            // console.log($("#item-category").val() != "default");
            // console.log('item cat');
            // console.log(data[8]);
            if($("#item-category").val() != "default")
            {
                var cat_id = data[8];
                if($("#item-category").val() == cat_id)
                {
                    result_cat = true;
                }
            }else
            {
                result_cat = true;
            }

            if(result_price && result_rank && result_weight && result_cat)
            {
                return true;
            }

            return false;
        }
    );

    //set category
   category.getCategory().then(function(cat){
        $scope.category = cat;
   });
    
    $http.get('http://c3mailer.com/amazonApp/product_json/results.json').then(successCallback, errorCallback);


    function successCallback(data){
    //success code
    	$scope.content = data;
	}

	function errorCallback(error){
	    //error code
        alert('fetching data failed');
	}

    // $timeout($scope.initTable);
    $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
        //you also get the actual event object
        //do stuff, execute functions -- whatever...
        if ( ! $.fn.DataTable.isDataTable( '#product-list' ) ) {
            main_table = $("#product-list").DataTable({
                "columnDefs": [
                    { "width": "60%", "targets": 3 },
                    {
                        "targets": [ 5 ],
                        "visible": false
                    },
                    {
                        "targets": [ 6 ],
                        "visible": false
                    },
                    {
                        "targets": [ 7 ],
                        "visible": false
                    },
                ],
            });
        }

        $("#btn-search-prod").click(function(){
            
            main_table.search($("#item-title").val()).draw();
        });

    });

    //modal function
    $scope.add_project_item = function(id){
        $('#projectModal').modal('toggle');
        $("#project-item-to-add").val(id);
    };
    
    $scope.project_save = function(project_id,project_name){
        var item_id = $("#project-item-to-add").val();
        $('#projectModal').modal('toggle');
        notie.alert({ type: 'warning', text: 'adding item to project ('+project_name+')...', stay:true });
        project.addItem(project_id,item_id).then(function(response){
            if(response.status)
                notie.alert({ type: 'success', text: 'item successfully add to the project ('+project_name+').', time:2 });
            else
                notie.alert({ type: 'error', text: 'item already add in the project ('+project_name+').', time:2 });
        });
    };

}).directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});;

app.filter("trust", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);

