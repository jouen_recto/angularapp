var first_time_log = true;
var securityService = angular.module('securityService', [])
.service('session', function (base_url,request_url,$http,$window) {
    var config = {
        headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
    };
    this.setSession = function (name,data) {
        $window.sessionStorage.setItem(name,data);
    };
    this.getSession = function (){
      return JSON.parse($window.sessionStorage);
    };
    this.clear = function (name){
      $window.sessionStorage.removeItem(name);
    };
    this.isLogin = function (title){
      var user_data = $window.sessionStorage.user_data;
     
      if(user_data != undefined)
      {
        user_data = JSON.parse(user_data);

        if(user_data.status == true)
        {

          if(title == 'login'){
            $window.location.href = base_url+'product';
          }
          else{
            $("#ng-user-name").html(user_data.user_data.name);
            var data = $.param({
                user_id:user_data.user_data.id
            });

            if(first_time_log)
            {
              first_time_log = false;
              $http.post(request_url+'Ngrequest/getProject',data,config).then(function (response) {
                  var data = response.data;
                  if(data.status)
                  {
                    var last_child = $("#display-projects li:last-child");
                    data.projectList.forEach(function(project, index){
                      last_child.before('<li><a href="#/project/'+project.id+'">'+project.name+'</a></li>');
                    });
                  }
              });
            }
          }

        }else
        {
          if(title != 'login')
            $window.location.href = base_url;
        }
        
      }else
      {
        if(title != 'login')
          $window.location.href = base_url;
      }
    };
});
